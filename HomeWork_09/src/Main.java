public class Main {
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(8);
        integerList.add(10);
        integerList.add(11);
        integerList.add(13);
        integerList.add(11);
        integerList.add(15);
        integerList.add(-5);


        integerList.remove(11);

        List<Integer> integerList1 = new ArrayList<>();
        integerList1.add(8);
        integerList1.add(10);
        integerList1.add(11);
        integerList1.add(13);
        integerList1.add(11);
        integerList1.add(15);
        integerList1.add(-5);


        integerList1.removeAt(4);

        System.out.println("Первый список после реализации метода:");
        for (int i = 0; i < integerList.size(); i++) {
            System.out.println(integerList.get(i));
        }

        System.out.println("Второй список после реализации метода:");
        for (int i = 0; i < integerList1.size(); i++) {
            System.out.println(integerList1.get(i));
        }
    }
    }
