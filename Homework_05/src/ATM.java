public class ATM {
    //сумма оставшихся денег в банкомате
    int balance;
    //максимальная сумма, разрешенная к выдачи
    int cashlimit;
    //максимальный возможный объем денег в банкомате
    int maxbalanceatm;
    //количество проведенных операций
    int numberoperation;
    //сумма которую хотят снять
    int sumcashissue;
    //сумма пополнения
    int sumincash;
    //сдача
    int returnmoney;


    public void cashissue() {
        if (sumcashissue < balance && sumcashissue < cashlimit) {
            balance = balance - sumcashissue;
            System.out.println("Сумма снятия: " + sumcashissue);
            System.out.println("Текущий баланс: " + balance);
        } else if (sumcashissue > balance) {
            System.out.println("Недостаточно средств");
        } else if (sumcashissue > cashlimit) {
            System.out.println("Превышет лимит на снятие средств");
        }

    }

    public void incash() {
        balance = balance + sumincash;
        if (balance <= maxbalanceatm) {
            System.out.println("Средства внесены! Ваш баланс:" + balance);
        } else {
            returnmoney = balance - maxbalanceatm;
            System.out.println("Непринятые средства: " + returnmoney);
            System.out.println("Ваш баланс: " + maxbalanceatm);
        }
    }


}
