drop table if exists car cascade;
drop table if exists driver cascade;
drop table if exists ride cascade;

create table driver
(
    id               serial primary key,
    first_name       varchar(20)                                           default 'DEFAULT_NAME',
    last_name        varchar(20)                                           default 'DEFAULT_NAME',
    phone_number     varchar(10),
    experience       integer check (experience >= 0 and experience <= 60 ) default 0,
    age              integer check (age >= 20 and age <= 81) not null,
    have_license     bool                                    not null,
    license_category varchar(3)                                            default 'B',
    rating           integer check (rating >= 0 and rating <= 5)           default 0
);

create table car
(
    id       serial primary key,
    model    varchar(20)       not null,
    color    varchar(20) default 'Сolorless car',
    number   varchar(9) unique not null,
    owner_id integer           not null,
    foreign key (owner_id) references driver (id)
);


create table ride
(
    driver_id     integer not null,
    car_id        integer not null,
    ride_date     timestamp,
    ride_duration time,
    foreign key (driver_id) references driver (id),
    foreign key (car_id) references car (id)
);