insert into driver (first_name, last_name, phone_number, experience, age, have_license, license_category, rating)
values ('Ильдар', 'Мингалиев', '8962569359', 10, 29, true, 'B,C', 5);

insert into driver (phone_number, age, have_license)
values ('8917555979', 48, true),
       ('8919255336', 20, false);

insert into driver (phone_number, experience, age, have_license, license_category, rating)
values ('8962555994', 3, 23, true, 'B', 5);

insert into driver (age, have_license)
values (27, false);

insert into car (model, color, number, owner_id)
values ('Tayota Rav4', 'черный', 'х568яя116', 1),
       ('Nissan Q', 'черный', 'х155яя116', 3),
       ('Shevrolet', 'красный', 'х333яя116', 4),
       ('Mercedes cla 200', 'фиолетовый', 'х051яя116', 1);

insert into car (model, number, owner_id)
values ('BWB X5', 'к636кк116', 2);


insert into ride (driver_id, car_id, ride_date, ride_duration)
values (1, 4, '2022-09-30', '9:00');

insert into ride (driver_id, car_id)
values (1, 1),
       (2, 3),
       (3, 3);
insert into ride(driver_id, car_id, ride_duration)
values (4, 3, '09:30')


