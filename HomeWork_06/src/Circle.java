public class Circle extends Figure {
    protected double radius;

    public Circle(int x, int y, int x1, int y1, double radius) {
        super(x, y, x1, y1);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
    @Override
    public void perimetr () {
        double p = Math.PI * (2 * radius);
        System.out.println("P =" + p);
    }
    @Override
    public void area () {
        double s = Math.PI * Math.pow(radius, 2);
        System.out.println("S =" + s);
    }
}