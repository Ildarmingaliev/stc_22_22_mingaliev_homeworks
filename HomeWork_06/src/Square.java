public class Square extends Figure {
    protected double sideorwidth;

    public Square(int x, int y, int x1, int y1, double sideorwidth) {
        super(x, y, x1, y1);
        this.sideorwidth = sideorwidth;
    }

    public double getSideorwidth() {

        return sideorwidth;
    }

    @Override
    public void perimetr() {
        double p = 4 * sideorwidth;
        System.out.println("P =" + p);
    }

    @Override
    public void area() {
        double s = Math.pow(sideorwidth, 2);
        System.out.println("S =" + s);
    }
}
