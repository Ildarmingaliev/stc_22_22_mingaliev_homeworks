public class Rectangle extends Square {
    private double height;

    public Rectangle(int x, int y, int x1, int y1, double sideorwidth, double height) {
        super(x, y, x1, y1, sideorwidth);
        this.height = height;
    }

    public double getHeight() {

        return height;
    }

    @Override
    public void perimetr() {
        double p = 2 * (sideorwidth + height);
        System.out.println("P =" + p);
    }

    @Override
    public void area() {
        double s = sideorwidth * height;
        System.out.println("S =" + s);
    }


}