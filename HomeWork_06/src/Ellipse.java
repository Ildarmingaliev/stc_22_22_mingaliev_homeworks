public class Ellipse extends Circle {
    private double bigradius;

    public Ellipse(int x, int y, int x1, int y1,double radius, double bigradius) {
        super(x, y, x1, y1, radius);
        this.bigradius = bigradius;
    }

    public double getBigradius() {

        return bigradius;
    }

    @Override
    public void perimetr() {
        double p = 2 * Math.PI * Math.sqrt( (Math.pow(radius,2) + Math.pow(bigradius, 2)) / 2);
        System.out.println("P =" + p);
    }

    @Override
    public void area() {
        double s = Math.PI * radius * bigradius;
        System.out.println("S =" + s);
    }
}
