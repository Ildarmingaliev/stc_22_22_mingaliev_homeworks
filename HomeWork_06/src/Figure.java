public abstract class Figure {
    protected int x;
    protected int y;
    protected int x1;
    protected int y1;

    public Figure(int x, int y, int x1, int y1) {
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
    }

    public int getX() {

        return x;
    }

    public double getY() {

        return y;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public abstract void perimetr();

    public abstract void area();

    public void move() {
        x = x1;
        y = y1;
        System.out.println("x=" + x + " y=" + y);
    }
}
