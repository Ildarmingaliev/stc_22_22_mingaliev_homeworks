public class Main {
    public static void main(String[] args) {
        Square square = new Square(2,3,4,5,5);
        System.out.println("Square");
        square.perimetr();
        square.area();
        square.move();

        Rectangle rectangle = new Rectangle(2,3,5,5,5,10);
        System.out.println("Rectangle");
        rectangle.perimetr();
        rectangle.area();
        rectangle.move();

        Circle circle = new Circle(2,3,5,5,5);
        System.out.println("Circle");
        circle.perimetr();
        circle.area();
        circle.move();

        Ellipse ellipse = new Ellipse(2, 3, 5, 5, 5,10);
        System.out.println("Ellipse");
        ellipse.perimetr();
        ellipse.area();
        ellipse.move();
    }
}
