import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int i = 0, count = 0, left = 0, middle = 0, right = 0;

        //Вот этого условия не хватало. Записал в таком ввиде, через i индекс не работает код. В комментарии укажите
        //пожалуйста корректное рабочее условие. Спасибо
        if ( array [0] < array [1]) {
            System.out.println("Локальный минимум левый: " + array[i]);
            count++;
        }


        while (i != array.length) {
            left = middle;
            middle = right;
            right = array[i++];
            if (right > middle && middle < left) {
                System.out.println("Локальный минимум: " + middle);
                count++;
            }
        }
        if (i > 1 && right < middle) {
            System.out.println("Локальный минимум правый: " + right);
            count++;
        }


        System.out.println("Количество локальных минимумов: " + count);
        count++;
    }

}


