import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    public static int calcSum(int left, int right) {
        int sum = 0;
        if (left > right) {
            sum = -1;
        }
        while (left <= right) {
            sum += left;
            left++;
        }
        return sum;

    }

    public static void isEven(int[] a, int from, int to) {
        for (int i = from; i <= to; i++) {
            if (a[i] % 2 == 0) {
                System.out.println(a[i]);
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите левую границу интервала:");
        int a = scanner.nextInt();
        System.out.println("Введите правую границу интервала:");
        int b = scanner.nextInt();
        int sum1 = calcSum(a, b);
        System.out.println("Сумма возращающихся чисел: " + sum1);

        System.out.println("Введите рамер массива:");
        int length = scanner.nextInt();
        System.out.println("Введите эллемнты массива");
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Четные числа массива:");
        isEven(array, 0, length - 1);

    }

}
