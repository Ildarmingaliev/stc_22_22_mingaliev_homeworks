import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ProductsRepository productsRepository = new ProductRepositoryFileBasedImpl("products.txt");

        System.out.println(productsRepository.findById(5));

        System.out.println(productsRepository.findAllByTitleLike("оло"));

    }
}