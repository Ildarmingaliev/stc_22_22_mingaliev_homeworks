public class UnsuccessfulWorkWithReaderException extends RuntimeException {
    public UnsuccessfulWorkWithReaderException(Exception e) {
        super(e);
    }
}
