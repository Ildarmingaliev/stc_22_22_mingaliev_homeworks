public class Product {
    private int id;
    private String title;
    private double price;
    private int quantityInStock;


    public Product(int id, String title, double price, int quantityInStock) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.quantityInStock = quantityInStock;
    }

    public void setId(int id) {

        this.id = id;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public void setPrice(double price) {

        this.price = price;
    }

    public void setQuantityInStock(int quantityInStock) {

        this.quantityInStock = quantityInStock;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", quantityInStock=" + quantityInStock +
                '}';
    }
}
