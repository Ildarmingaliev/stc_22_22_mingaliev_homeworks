public class Main {
    public static void completeAllTask(Task[] tasks) {
        for (Task task : tasks) {
            task.complete();
        }
    }

    public static void main(String[] args) {
        EvenNumbersPrintTask e1 = new EvenNumbersPrintTask(2, 15);
        EvenNumbersPrintTask e2 = new EvenNumbersPrintTask(3, 9);
        EvenNumbersPrintTask e3 = new EvenNumbersPrintTask(34, 43);
        EvenNumbersPrintTask e4 = new EvenNumbersPrintTask(5, 24);

        OddNumbersPrintTask o1 = new OddNumbersPrintTask(0, 13);
        OddNumbersPrintTask o2 = new OddNumbersPrintTask(5, 23);
        OddNumbersPrintTask o3 = new OddNumbersPrintTask(11, 17);
        OddNumbersPrintTask o4 = new OddNumbersPrintTask(69, 82);

        Task[] tasks = {e1, e2, e3, e4, o1, o2, o3, o4};
        completeAllTask(tasks);

    }
}