public class Main {
    public static void main(String[] args) {

        ArrayTask sumArrayFromTo = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            return sum;
        };

        ArrayTask sumOfDigitsofMaxElement = (array, from, to) -> {
            int maxnumber = 0;
            int x;
            int sum = 0;
            for (int i = from; i <= to; i++) {
                if (array[i] > maxnumber) {
                    maxnumber = array[i];
                }
            }
            x = maxnumber;
            while (x > 0) {
                sum += x % 10;
                x /= 10;
            }
            return sum;
        };
        int[] array1 = {25, 5, 99, 666, 36};
        int from1 = 0;
        int to1 = 4;

        ArraysTasksResolver.resolveTask(array1, sumArrayFromTo, from1, to1);

        int[] array2 = {55, 163, 32, 6, 86};
        int from2 = 1;
        int to2 = 4;

        ArraysTasksResolver.resolveTask(array2, sumOfDigitsofMaxElement, from2, to2);


    }
}